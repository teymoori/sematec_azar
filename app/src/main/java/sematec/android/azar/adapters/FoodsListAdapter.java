package sematec.android.azar.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import sematec.android.azar.R;
import sematec.android.azar.models.FoodModel;

/**
 * Created by amirhossein on 12/16/17.
 */

public class FoodsListAdapter extends BaseAdapter {
    Context mContext;
    List<FoodModel> foods;


    public FoodsListAdapter(Context mContext, List<FoodModel> foods) {
        this.mContext = mContext;
        this.foods = foods;
    }

    @Override
    public int getCount() {
        return foods.size();
    }

    @Override
    public Object getItem(int position) {
        return foods.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        View rowView = LayoutInflater.from(mContext).inflate(R.layout.foods_list_item, viewGroup, false);

        ImageView img = rowView.findViewById(R.id.img);
        TextView foodName = rowView.findViewById(R.id.foodName);
        TextView foodPrice = rowView.findViewById(R.id.foodPrice);

        foodName.setText(foods.get(position).getName());

        foodPrice.setText(foods.get(position).getPrice() + "");


        Picasso.with(mContext).load(foods.get(position).getImageURL()).into(img);


        return rowView;
    }
}
