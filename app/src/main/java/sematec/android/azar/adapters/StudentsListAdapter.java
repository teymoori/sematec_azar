package sematec.android.azar.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import sematec.android.azar.R;

/**
 * Created by amirhossein on 12/16/17.
 */

public class StudentsListAdapter extends BaseAdapter {

    Context mContext ;
    String names[] ;

    public StudentsListAdapter(Context mContext, String[] names) {
        this.mContext = mContext;
        this.names = names;
    }

    @Override
    public int getCount() {
        return names.length;
    }

    @Override
    public Object getItem(int position) {
        return names[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        View rowView = LayoutInflater.from(mContext).inflate(R.layout.student_list_item, viewGroup, false);

        TextView studentName = (TextView) rowView.findViewById(R.id.studentName) ;

        studentName.setText( names[position] );



        return rowView;
    }
}
