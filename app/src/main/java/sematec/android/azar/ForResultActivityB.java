package sematec.android.azar;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class ForResultActivityB extends AppCompatActivity {
    EditText name ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_for_result_b);

        name = (EditText) findViewById(R.id.name) ;




        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent result = new Intent() ;
                result.putExtra("name" , name.getText().toString()) ;
                setResult(Activity.RESULT_OK,result);
                finish();
            }
        });

    }
}
