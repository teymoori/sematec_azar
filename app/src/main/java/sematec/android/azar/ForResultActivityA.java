package sematec.android.azar;

import android.content.Intent;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class ForResultActivityA extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_for_result_activity);


        findViewById(R.id.goToNextActivity).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent openNextActivity = new Intent(mContext, ForResultActivityB.class);

                startActivityForResult(openNextActivity, 189);
//                startActivityForResult(openNextActivity, 189);

               // Intent gpsSetting = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS) ;
              //  startActivityForResult(gpsSetting, 189);
            }
        });
    }
     
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 189) {
            String resultName = data.getStringExtra("name");
            Toast.makeText(mContext, resultName, Toast.LENGTH_SHORT).show();
        }

    }
}
