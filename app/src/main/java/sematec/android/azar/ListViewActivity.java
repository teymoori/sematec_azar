package sematec.android.azar;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.BaseAdapter;
import android.widget.ListView;

import sematec.android.azar.adapters.StudentsListAdapter;

public class ListViewActivity extends BaseActivity {
    ListView studentsList ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view_acitity);

        studentsList = (ListView) findViewById(R.id.studentsList) ;


        String names[] = {
                "Alireza" ,
                "Alireza" ,
                "Mahmood" ,
                "Mahmood" ,
                "Mahmood" ,
                "Hasan" ,
                "Hasan" ,
                "Maryam" ,
                "Maryam" ,
                "Akbar",
                "Akbar"
        };

        StudentsListAdapter adapter = new StudentsListAdapter(mContext , names) ;


        studentsList.setAdapter(adapter);



    }
}
