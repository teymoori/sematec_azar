package sematec.android.azar;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

public class FormDestinationActivity extends BaseActivity{
    TextView result ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_destination);

        result = (TextView) findViewById(R.id.result) ;


        String name = getIntent().getStringExtra("name") ;

        result.setText(name);
    }
}
