package sematec.android.azar;

import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.orhanobut.hawk.Hawk;

public class SharedActivity extends BaseActivity implements View.OnClickListener {
    EditText username;
    EditText password;
    Button save;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shared);
        Hawk.init(mContext).build();
        bindViews();

        username.setText(getHawk("username", ""));
        password.setText(getHawk("password", ""));
    }

    private void bindViews() {
        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);
        save = (Button) findViewById(R.id.save);
        save.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.save) {
            if (username.getText().toString().length() == 0 ||
                    password.getText().toString().length() == 0
                    ) {
                Toast.makeText(mContext, "please fill all fields", Toast.LENGTH_SHORT).show();
            } else {

                setShared("username", username.getText().toString());
                setShared("password", password.getText().toString());
                username.setText("");
                password.setText("");
                Toast.makeText(mContext, "thank u", Toast.LENGTH_SHORT).show();
            }

        }
    }


    void setShared(String key, String value) {
        //by hawk library'
        Hawk.put(key, value);

        //manually
        PreferenceManager.getDefaultSharedPreferences(mContext).
                edit().putString(key, value)
                .apply();
    }

    String getHawk(String key, String defValue) {
        String result = Hawk.get(key, defValue);
        return result;
    }

    String getShared(String key, String defValue) {
        //manually
        return PreferenceManager.getDefaultSharedPreferences(mContext).
                getString(key, defValue);
    }
}
