package sematec.android.azar;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class FormActivity extends BaseActivity implements View.OnClickListener {

    EditText username;
    EditText password;
    Button show;
    TextView result;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form);
        bindViews();


//        show.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//
//                String usernameValue = username.getText().toString() ;
//                String passwordValue = password.getText().toString() ;
//                result.setText( usernameValue + " " + passwordValue );
//                username.setText("");
//                password.setText("");
//                Toast.makeText(mContext, usernameValue, Toast.LENGTH_LONG).show();
//            }
//        });


    }


    void bindViews() {
        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);
        show = (Button) findViewById(R.id.show);
        result = (TextView) findViewById(R.id.result);
        show.setOnClickListener(this);
        result.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.show) {
            String usernameValue = username.getText().toString();
            String passwordValue = password.getText().toString();
            result.setText(usernameValue + " " + passwordValue);

            Toast.makeText(mContext, usernameValue, Toast.LENGTH_LONG).show();

            //send values to formDestination activity
            Intent destinationIntent = new Intent(mContext, FormDestinationActivity.class);
            destinationIntent.putExtra("name", usernameValue);
            destinationIntent.putExtra("family", "hasani");
            startActivity(destinationIntent);

            username.setText("");
            password.setText("");
        } else if (view.getId() == R.id.result) {
            //
        }

    }
}
