package sematec.android.azar;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

public class PicassoLibraryActivity extends BaseActivity {
    ImageView img;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_picasso_library);

        img = (ImageView) findViewById(R.id.img);


        Picasso.with(mContext).load("https://www.irib.ir/assets/slider_images/20171122101141_1124.png").into(img);

    }
}
