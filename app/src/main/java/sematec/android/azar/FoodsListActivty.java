package sematec.android.azar;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import sematec.android.azar.adapters.FoodsListAdapter;
import sematec.android.azar.models.FoodModel;

public class FoodsListActivty extends AppCompatActivity {
    ListView foodsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_foods_list_activty);


        foodsList = (ListView) findViewById(R.id.foodsList);

        FoodModel ghormesabzi = new FoodModel();
        ghormesabzi.setName("قرمه سبزی");
        ghormesabzi.setPrice(12500);
        ghormesabzi.setType("Sonnati");
        ghormesabzi.setImageURL("https://www.2nafare.com/wp-content/uploads/2015/01/%D9%82%D9%88%D8%B1%D9%85%D9%87-%D8%B3%D8%A8%D8%B2%DB%8C-%D8%AC%D8%A7-%D8%A7%D9%81%D8%AA%D8%A7%D8%AF%D9%87.jpg");

        FoodModel gheyme = new FoodModel();
        gheyme.setName("قیمه");
        gheyme.setPrice(8500);
        gheyme.setType("Sonnati");
        gheyme.setImageURL("http://cdn.asriran.com/files/fa/news/1396/7/5/743455_284.jpg");


        FoodModel pitza = new FoodModel();
        pitza.setName("پیتزا");
        pitza.setPrice(25000);
        pitza.setType("FastFood");
        pitza.setImageURL("https://blog-admin.reyhoon.com/wp-content/images/2017/08/%D8%B3%D9%81%D8%A7%D8%B1%D8%B4-%D9%BE%DB%8C%D8%AA%D8%B2%D8%A7.jpg");


        List<FoodModel> foods = new ArrayList<>();


        foods.add(gheyme);
        foods.add(pitza);
        foods.add(ghormesabzi);


        FoodsListAdapter adapter = new FoodsListAdapter(this, foods);


        foodsList.setAdapter(adapter);



        foodsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

            }
        });


    }
}
